import numpy as np


def parse_input(data):
    """
    Converts from string to matrix
    """
    lines = data.split("\n")
    out = []
    for line in lines:
        if len(line) < 10:
            continue
        cur_row = []
        line = line.lstrip().rstrip()
        for j, char in enumerate(line):
            cur_row.append(int(char))
        out.append(cur_row)
    return np.array(out, np.uint32)


def find_min_cost_divide_and_conquer(matrix, row, col):
    """
    Assuming can only go left or down to explore Divide and Conquer
    Looking at recursive Divide and Conquer from
    https://pythonwife.com/divide-and-conquer-algorithm-in-python/
    She seems to run it back from the end to start, I'll reverse it

    Was interested to try this, but apparently it's not efficient
    enough.
    """
    # Returns last element
    if row == matrix.shape[0] - 1 and col == matrix.shape[1] - 1:
        return matrix[row][col]
    # Makes sure walking off the end is never selected
    elif row >= matrix.shape[0] or col >= matrix.shape[1]:
        return float("inf")
    # Breaks the problem recursively for row and col
    else:
        op1 = find_min_cost_divide_and_conquer(matrix, row + 1, col)
        op2 = find_min_cost_divide_and_conquer(matrix, row, col + 1)
        return matrix[row][col] + min(op1, op2)


def accumulate_in_grid(mat):
    """
    For each number of steps you just want to find the minimum
    cost to get that many steps.

    Still assuming I can only go left or down at each step...
    https://hackernoon.com/minimum-cost-path-analysis-python-47ad79a54519

    Result was too high, assumption must be bad
    """
    # Do outer row and columns
    for i in range(2, mat.shape[0]):  # skips first element
        mat[0, i] += mat[0, i - 1]
    for j in range(2, mat.shape[1]):
        mat[j, 0] += mat[j - 1, 0]
    # Fill in each position with the lowest way to get there.
    for i in range(1, mat.shape[0]):
        for j in range(1, mat.shape[1]):
            mat[i, j] += min(mat[i - 1, j], mat[i, j - 1])
    return mat[-1, -1]


def a_star_search_algorithm(mat):
    """
    One mo' time
    https://www.linkedin.com/pulse/least-cost-path-analysis-algorithm-chonghua-yin/
    """
    pass


if __name__ == "__main__":
    test1 = """
        1163751742
        1381373672
        2136511328
        3694931569
        7463417111
        1319128137
        1359912421
        3125421639
        1293138521
        2311944581
    """
    mat = parse_input(test1)
    res = accumulate_in_grid(mat)
    print("Final result = " + str(res))

    with open("15_part1.txt", "r") as fh:
        part1 = fh.read()
    mat = parse_input(part1)
    res = accumulate_in_grid(mat)
    print("Part 1 = " + str(res))
