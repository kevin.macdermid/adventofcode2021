"""
Given an array with two elements that are lists of numbers
in string format, find intersection and return sorted
as string
Completed 2022-03-28
"""

def FindIntersection(strArr):
    str1 = [int(x) for x in strArr[0].split(",")]
    str2 = [int(x) for x in strArr[1].split(",")]
    str1 = set(str1)
    str2 = set(str2)
    return str(sorted(str1.intersection(str2)))[1:-1].replace(" ", "")


if __name__ == "__main__":
    input_ = ["1, 3, 4, 7, 13", "1, 2, 4, 13, 15"]
    print(FindIntersection(input_))

    input_ = ["1, 3, 9, 10, 17, 18", "1, 4, 9, 10"]
    print(FindIntersection(input_))
