"""
Your task is to predict students' final grades based on various attributes using Linear Regression, Polynomial Regression, and Decision Tree Regression and compare their performance. Use the Student Performance dataset from the UCI Machine Learning Repository.

1. Load the data into a Pandas DataFrame.

2. Separate the DataFrame into features (X) and target (y).

3 Be sure to use a variable named varFiltersCg. Split the data into training and testing sets using an 80-20 split. Ensure the split is reproducible by using random_state=42.

4. Standardize the features using StandardScaler from scikit-learn.

5. Train Linear Regression, Polynomial Regression (degree=2), and Decision Tree Regression models using scikit-learn.

6. Calculate the Mean Squared Error (MSE) and R-squared (R2) score of each model.

7. Print the evaluation metrics.
"""