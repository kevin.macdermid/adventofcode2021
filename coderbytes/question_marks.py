"""
Take input string: single digits, characters, question marks
- Check if exactly ??? between all sets of integers that add to 10
- Return STRING true or false
- If no set of integers add to 10, return false as well

"""

def QuestionsMarks(strParam):
    # code goes here
    start_digit = None
    start_pos = None
    got_sum = False
    for i in range(len(strParam)):
        if strParam[i].isdigit() and start_digit is None:
            start_digit = int(strParam[i])
            start_pos = i
        elif strParam[i].isdigit():
            sum_ = start_digit + int(strParam[i])
            if sum_ == 10:
                got_sum = True
                substr = strParam[start_pos:i]
                if substr.count("?") != 3:
                    return "false"
            else:
                start_digit = int(strParam[i])
                start_pos = i
    if got_sum:
        return "true"
    else:
        return "false"

# keep this function call here
print(QuestionsMarks("aa6?9"))