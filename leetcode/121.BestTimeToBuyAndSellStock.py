import numpy as np


def max_profit_leetcode(prices):
    """
    Here's a simple solution from the discussion board
    Also Kadane's algorithm could work
    """
    max_profit, min_price = 0, float('inf')
    for price in prices:
        min_price = min(min_price, price)
        profit = price - min_price
        max_profit = max(max_profit, profit)
    return max_profit


def max_profit_accum(prices):
    """
    Works, still in the slowest 5%
    """
    # Go from start side of the prices array -keep track of min so far
    min_ = np.array([prices[0]] * len(prices))
    for i in range(1, len(prices)):
        if prices[i] < min_[i-1]:
            min_[i] = prices[i]
        else:
            min_[i] = min_[i-1]

    # Go from back end of the prices array - keep track of max so far
    max_ = np.array([prices[-1]] * len(prices))
    for i in range(len(prices)-2, -1, -1):
        if prices[i] > max_[i+1]:
            max_[i] = prices[i]
        else:
            max_[i] = max_[i+1]

    # Compute deltas
    max_diff = np.max(max_ - min_)

    # Return max
    if max_diff > 0:
        return max_diff
    else:
        return 0


def max_profit_deltas(prices):
    """
    Still too slow
    """
    prices = np.array(prices)
    max_profit = 0
    for i in range(1, len(prices)):
        deltas = prices[i:] - prices[:-i]
        if max(deltas) > max_profit:
            max_profit = max(deltas)
    return max_profit


def max_profit_loop(prices):
    """
    Have a list of prices, want to buy at lowest
    and sell at highest, highest has to follow lowest
    Basic algorithm - this probably won't be efficient enough
    """
    prices = np.array(prices)
    sprices = sorted(prices)
    max_profit = 0
    for min_val in sprices[:-1]:
        min_ind = np.where(prices == min_val)[0][0]  # find first index of min value, first is always the best one
        max_val = np.max(prices[min_ind:])
        if max_val - min_val > max_profit:
            max_profit = max_val - min_val
    return max_profit


def determine_maximum_profit(prices):
    return max_profit_accum(prices)


if __name__ == "__main__":
    assert determine_maximum_profit([7, 1, 5, 3, 6, 4]) == 5
    assert determine_maximum_profit([7, 6, 4, 3, 1]) == 0
    assert determine_maximum_profit([2, 4, 1]) == 2
