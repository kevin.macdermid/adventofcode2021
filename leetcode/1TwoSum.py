"""
Given a list, find the indices of the elements that
sum to the target.

Solved: 2022-03-29
"""


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for first_ind in range(len(nums) - 1):
            for sec_ind in range(first_ind + 1, len(nums)):
                sum_ = nums[first_ind] + nums[sec_ind]
                if sum_ == target:
                    return [first_ind, sec_ind]
