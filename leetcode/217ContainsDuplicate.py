

def contains_duplicate(nums):
    nset = set(nums)
    return len(nset) < len(nums)


if __name__ == "__main__":
    assert contains_duplicate([1,2,3,1])
    assert not contains_duplicate([1,2,3,4])

