# This is support fn they have in theirs
def isBadVersion(version, cutoff):
    if version >= cutoff:
        return True
    else:
        return False


def main(n, cutoff):
    """
    Determine first version in set of version numbers that is bad
    Another binary search
    Inspired by GeeksForGeeks binary search iterative.
    """
    low = 0
    high = n

    while low < high:
        mid = (high + low) // 2
        # Bad version is earlier
        if isBadVersion(mid+1, cutoff):  # (Version is 1, 2, 3, ...)
            high = mid-1
        # Bad version is later
        elif not isBadVersion(mid+1, cutoff):
            low = mid+1
    # On last element, could be first bad version or not
    if isBadVersion(low+1, cutoff):
        return low+1
    else:
        return low+2


if __name__ == "__main__":
    # Unittests go here
    assert main(5, 4) == 4
    assert main(1, 1) == 1
    assert main(2, 2) == 2
