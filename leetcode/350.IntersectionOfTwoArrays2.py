
def intersection_of_two_arrays(nums1, nums2):
    """
    Do intersection, but also keep every repeated time it shows up
    """
    out = []
    for val in nums2:
        for i in range(len(nums1)):
            if val == nums1[i]:
                nums1.pop(i)
                out.append(val)
                break
    return out


if __name__ == "__main__":
    print(intersection_of_two_arrays([1,2,2,1], [2,2]))
    print(intersection_of_two_arrays([4,9,5], [9,4,9,8,4]))

