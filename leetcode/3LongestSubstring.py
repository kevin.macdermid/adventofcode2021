"""
Given a string, find the length of the longest
substring without repeating characters

Solved: 2022-03-29
"""


def find_longest_substring(s:str):
    # With pos as start index
    # Append to current substring
    # Keep track of maximum length
    # Step until you find a repeated character
    # Start pos is just after first occurance of character
    # pos -
    pos = 0
    substr = ""
    max_len = 0
    # ToDo: Could optimize not to check last segment
    while pos < len(s):
        char = s[pos]
        if char not in substr:
            substr += char
        else:
            # Hit repetition character
            # Substring is longest segment that doesn't contain current char
            substr = substr.split(char)[1] + char
        if len(substr) > max_len:
            max_len = len(substr)
        pos += 1

    return max_len


if __name__ == "__main__":
    # Unittests
    assert find_longest_substring("abcabcbb") == 3
    assert find_longest_substring("bbbbb") == 1
    assert find_longest_substring("pwwkew") == 3
    assert find_longest_substring("dvdf") == 3

