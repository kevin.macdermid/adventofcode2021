import numpy as np

"""
Find contiguous subarray which has the largest sum
and return its sum
"""

def find_subarray_iterate_all(nums):
    # Check every subarray one by one - too slow
    nums = np.array(nums)
    sums = []
    for start_ind in range(len(nums)):
        # Need +1 here as range doesn't include endpoint
        for end_ind in range(start_ind+1, len(nums)+1):
            sums.append(np.sum(nums[start_ind:end_ind]))
    return max(sums)


def find_subarray_grow(nums):
    """
    Growing each sum on the right, should be doing fewer calculations.

    Timed, took 1/10 the time of above. Still not fast enough...
    """
    nums = np.array(nums)
    max_sum = max(nums)  # start with single element subarrays
    cur_inds = range(len(nums))  # right indices, will grow on that side

    subarrs_in = [(x, y) for x, y in zip(nums[:-1], cur_inds[:-1])]
    subarrs_out = []
    n = 1
    while n < len(nums):
        for sub in subarrs_in:
            rind = sub[1] + 1
            sum_ = sub[0] + nums[rind]
            if sum_ > max_sum:
                max_sum = sum_
            subarrs_out.append((sum_, rind))
        n += 1
        subarrs_in = subarrs_out[:-1]  # throw away last one, can't grow on that side
        subarrs_out = []
    return max_sum


def find_subarray_kadane(nums):
    """
    Looking at the solution provided in discussions

    Apparently this is Kadane's Algorithm (Dynamic Programming)
    https://medium.com/@rsinghal757/kadanes-algorithm-dynamic-programming-how-and-why-does-it-work-3fd8849ed73d

    You can also set up a recursive solution with divide and conquer
    https://leetcode.com/problems/maximum-subarray/discuss/1595195/C%2B%2BPython-7-Simple-Solutions-w-Explanation-or-Brute-Force-%2B-DP-%2B-Kadane-%2B-Divide-and-Conquer
    """
    for i in range(1, len(nums)):
        # It's worthwhile to adding to existing total so long as our total is >0.
        # So we just keep a running sum in place in the original array (to minimize memory requirements)
        if nums[i-1] > 0:
            nums[i] += nums[i - 1]
    # Return final max over array
    return max(nums)


def find_subarray(nums):
    #return find_subarray_grow(nums)
    #return find_subarray_iterate_all(nums)
    return find_subarray_kadane(nums)


if __name__ == "__main__":
    import time
    assert find_subarray([-2, 1, -3, 4, -1, 2, 1, -5, 4]) == 6
    assert find_subarray([1]) == 1
    assert find_subarray([5,4,-1,7,8]) == 23
    t0 = time.time()
    assert find_subarray([-57,9,-72,-72,-62,45,-97,24,-39,35,-82,-4,-63,1,-93,42,44,1,-75,-25,-87,-16,9,-59,20,5,-95,-41,4,-30,47,46,78,52,74,93,-3,53,17,34,-34,34,-69,-21,-87,-86,-79,56,-9,-55,-69,3,5,16,21,-75,-79,2,-39,25,72,84,-52,27,36,98,20,-90,52,-85,44,94,25,51,-27,37,41,-6,-30,-68,15,-23,11,-79,93,-68,-78,90,11,-41,-8,-17,-56,17,86,56,15,7,66,-56,-2,-13,-62,-77,-62,-12,37,55,81,-93,86,-27,-39,-3,-30,-46,6,-8,-79,-83,50,-10,-24,70,-93,-38,27,-2,45,-7,42,-57,79,56,-57,93,-56,79,48,-98,62,11,-48,-77,84,21,-47,-10,-87,-49,-17,40,40,35,10,23,97,-63,-79,19,6,39,62,-38,-27,81,-68,-7,60,79,-28,-1,-33,23,22,-48,-79,51,18,-66,-98,-98,50,41,13,-63,-59,10,-49,-38,-70,56,77,68,95,-73,26,-73,20,-14,83,91,61,-50,-9,-40,1,11,-88,-80,21,89,97,-29,8,10,-15,48,97,35,86,-96,-9,64,48,-37,90,-26,-10,-13,36,-27,-45,-3,-1,45,34,77,-66,22,73,54,11,70,-97,-81,-43,-13,44,-69,-78,30,-66,-11,-29,58,52,-61,-68,-81,25,44,-32,57,-81,66,2,52,43,35,-26,16,-33,61,-37,-54,80,-3,32,24,27,30,-69,38,-81,2,-4,47,17,5,42,-58,-51,-90,98,-33,7])
    t1 = time.time()
    print(f"{t1-t0}")

