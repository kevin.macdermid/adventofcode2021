"""
Given two arrays sorted non-decrasing
m, n are lengths respectively.

Merge them into a single array that is sorted
in non decreasing.

Store output into existing nums1 (which is 0 padded
to fit the extra stuff)
"""


def merge_sorted_arrays(nums1, m, nums2, n):
    nums1[m:m+n] = nums2
    nums1.sort()


def merge_by_hand(nums1, m, nums2, n):
    """
    Stolen from discussion board, want
    to build from back end where zeroes are
    This is more efficient than the built in sort
    """
    # m+n is our current position in output array, m, n used to track each array
    while m > 0 and n > 0:
        if nums1[m-1] >= nums2[n-1]:
            # Puts the num1 value at our current position
            # This causes duplicates of the value for a while, gets overwritten later
            nums1[m+n-1] = nums1[m-1]
            m -= 1
        else:
            # Puts the nums2 value at our current position
            nums1[m+n-1] = nums2[n-1]
            n -= 1
    # Leftover in nums2 needs to be put at the start (nums1 already shifted up)
    if n > 0:
        nums1[:n] = nums2[:n]


if __name__ == "__main__":
    #merge_sorted_arrays([1,2,3,0,0,0], 3, [2,5,6], 3)
    merge_by_hand([1,2,3,0,0,0], 3, [2,5,6], 3)
